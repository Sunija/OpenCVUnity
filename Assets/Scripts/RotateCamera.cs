﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotateCamera : MonoBehaviour {

    public Transform target;
    public float speed;

    private float fieldOfViewInitial;

    private void Start()
    {
        fieldOfViewInitial = Camera.main.fieldOfView;
    }

    void Update()
    {
        Vector3 targetDir = target.position - transform.position;
        targetDir = targetDir.normalized * speed * Time.deltaTime;
        float step = speed * Time.deltaTime;
        Vector3 newDir = Vector3.RotateTowards(transform.forward, targetDir, step, 0.0F);
        Debug.DrawRay(transform.position, newDir, Color.red);
        transform.rotation = Quaternion.LookRotation(newDir);

        float fovScale = Object.FindObjectOfType<OpenCVFaceDetection>()._faces[0].Radius / 250f;
        Debug.Log(fovScale);

        Camera.main.fieldOfView = fieldOfViewInitial / fovScale;
    }
}
