﻿using System.Runtime.InteropServices;
using System.Collections;
using UnityEngine;

public class Job : ThreadedJob
{
    private int _maxFaceDetectCount = 5;

    private CvCircle[] _faces;

    protected override void ThreadFunction()
    {
        // Do your threaded task. DON'T use the Unity API here
        _faces = new CvCircle[_maxFaceDetectCount];

        int detectedFaceCount = 0;

        unsafe
        {
            fixed (CvCircle* outFaces = _faces)
            {
                OpenCVInterop.DetectSingleFace(outFaces, 320, false, ref detectedFaceCount);
            }
        }
        
    }
    protected override void OnFinished()
    {
        // This is executed by the Unity main thread when the job is finished
        if (_faces[0].X != 0)
            Object.FindObjectOfType<OpenCVFaceDetection>()._faces[0] = _faces[0];
    }
}

