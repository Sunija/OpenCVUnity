﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Runtime.InteropServices;

public class OpenCVFaceDetection : MonoBehaviour
{
    public static List<Vector2> NormalizedFacePositions { get; private set; }
    public static Vector2 CameraResolution;

    public float updateEvery = 0.2f;

    public bool isThreaded = true;

    private bool isStarted = false;

    private Job myJob;

    private float timer = 0f;

    /// <summary>
    /// Downscale factor to speed up detection.
    /// </summary>
    private const int DetectionDownScale = 1;

    public bool _ready;
    private int _maxFaceDetectCount = 5;
    public CvCircle[] _faces;

    

    void Start()
    {
        int camWidth = 0, camHeight = 0;
        int result = OpenCVInterop.Init(ref camWidth, ref camHeight);
        if (result < 0)
        {
            if (result == -1)
            {
                Debug.LogWarningFormat("[{0}] Failed to find cascades definition.", GetType());
            }
            else if (result == -2)
            {
                Debug.LogWarningFormat("[{0}] Failed to open camera stream.", GetType());
            }

            return;
        }

        CameraResolution = new Vector2(camWidth, camHeight);
        _faces = new CvCircle[_maxFaceDetectCount];
        NormalizedFacePositions = new List<Vector2>();
        OpenCVInterop.SetScale(DetectionDownScale);
        _ready = true;
    }

    void OnApplicationQuit()
    {
        if (_ready)
        {
            OpenCVInterop.Close();
        }
    }

    void Update()
    {
        int detectedFaceCount = 1;

        if (_ready)
        {
            if (isThreaded)
            {
                // start tracking thread
                if (!isStarted)
                {
                    StartTracking();
                    isStarted = true;
                }

                // check if job is finished, restart if necessary
                if (myJob != null)
                {
                    if (myJob.Update())
                    {
                        // Alternative to the OnFinished callback
                        myJob = null;
                        StartTracking();
                    }
                }
            }
            else
            {
                // detect
                unsafe
                {
                    fixed (CvCircle* outFaces = _faces)
                    {
                        OpenCVInterop.DetectSingleFace(outFaces, 320, false, ref detectedFaceCount);
                    }
                }
            }
        }
        


        // normalize face position
        NormalizedFacePositions.Clear();
        for (int i = 0; i < detectedFaceCount; i++)
        {
            Debug.Log("X " + _faces[i].X + " " + "Y " + _faces[i].Y + " " + "R " + _faces[i].Radius);
            NormalizedFacePositions.Add(new Vector2((_faces[i].X * DetectionDownScale) / CameraResolution.x, 1f - ((_faces[i].Y * DetectionDownScale) / CameraResolution.y)));
        }
        
        // close tracking, if playmode stopped
#if UNITY_EDITOR
        if (UnityEditor.EditorApplication.isPlaying == false)
        {
            if (myJob != null)
            {
                myJob.Abort();
            }
            if (_ready)
            {
                OpenCVInterop.Close();
            }
        }
#endif

    }

    void StartTracking()
    {
        myJob = new Job();
        myJob.Start(); // Don't touch any data in the job class after you called Start until IsDone is true.
    }

}


// Define the functions which can be called from the .dll.
internal static class OpenCVInterop
{
    [DllImport("OpenCVUnity")]
    internal static extern int Init(ref int outCameraWidth, ref int outCameraHeight);

    [DllImport("OpenCVUnity")]
    internal static extern int Close();

    [DllImport("OpenCVUnity")]
    internal static extern int SetScale(int downscale);

    [DllImport("OpenCVUnity")]
    internal unsafe static extern void Detect(CvCircle* outFaces, int maxOutFacesCount, bool isDebug, ref int outDetectedFacesCount);

    [DllImport("OpenCVUnity")]
    internal unsafe static extern void DetectSingleFace(CvCircle* outFaces, int frameResizeWidth, bool isDebug, ref int outDetectedFacesCount);
}
// Define the structure to be sequential and with the correct byte size (3 ints = 4 bytes * 3 = 12 bytes)
[StructLayout(LayoutKind.Sequential, Size = 12)]
public struct CvCircle
{
    public int X, Y, Radius;
}