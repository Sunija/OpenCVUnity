#include "VideoFaceDetector.h"

#include "opencv2/objdetect.hpp"
#include "opencv2/highgui.hpp"
#include "opencv2/imgproc.hpp"

#include <iostream>
#include <stdio.h>


using namespace std;
using namespace cv;

const cv::String    CASCADE_FILE("haarcascade_frontalface_default.xml");

// Declare structure to be used to pass data from C++ to Mono.
struct Circle
{
	Circle(int x, int y, int radius) : X(x), Y(y), Radius(radius) {}
	int X, Y, Radius;
};

CascadeClassifier _faceCascade;
String _windowName = "Unity OpenCV Interop Sample";
VideoCapture _capture;

int _scale = 1;

extern "C" int __declspec(dllexport) __stdcall  Init(int& outCameraWidth, int& outCameraHeight)
{
	// Load LBP face cascade.
	if (!_faceCascade.load("lbpcascade_frontalface.xml"))
		return -1;

	// Open the stream.
	_capture.open(0);
	if (!_capture.isOpened())
		return -2;

	outCameraWidth = _capture.get(CAP_PROP_FRAME_WIDTH);
	outCameraHeight = _capture.get(CAP_PROP_FRAME_HEIGHT);

	return 0;
}

extern "C" void __declspec(dllexport) __stdcall  Close()
{
	_capture.release();
}

extern "C" void __declspec(dllexport) __stdcall SetScale(int scale)
{
	_scale = scale;
}

extern "C" void __declspec(dllexport) __stdcall Detect(Circle* outFaces, int maxOutFacesCount, bool isDebug, int& outDetectedFacesCount)
{
	Mat frame;
	_capture >> frame;
	if (frame.empty())
		return;

	std::vector<Rect> faces;
	// Convert the frame to grayscale for cascade detection.
	Mat grayscaleFrame;
	cvtColor(frame, grayscaleFrame, COLOR_BGR2GRAY);
	Mat resizedGray;
	// Scale down for better performance.
	resize(grayscaleFrame, resizedGray, Size(frame.cols / _scale, frame.rows / _scale));
	equalizeHist(resizedGray, resizedGray);

	// Detect faces.
	_faceCascade.detectMultiScale(resizedGray, faces);
	
	
	// Draw faces.
	for (size_t i = 0; i < faces.size(); i++)
	{
		Point center(_scale * (faces[i].x + faces[i].width / 2), _scale * (faces[i].y + faces[i].height / 2));
		ellipse(frame, center, Size(_scale * faces[i].width / 2, _scale * faces[i].height / 2), 0, 0, 360, Scalar(0, 0, 255), 4, 8, 0);

		// Send to application.
		outFaces[i] = Circle(faces[i].x, faces[i].y, faces[i].width / 2);
		outDetectedFacesCount++;

		if (outDetectedFacesCount == maxOutFacesCount)
			break;
	}

	// Display debug output.
	if (isDebug)
	{
		imshow(_windowName, frame);
	}
}

extern "C" void __declspec(dllexport) __stdcall DetectSingleFace(Circle* outFaces, int frameResizeWidth, bool isDebug, int& outDetectedFacesCount)
{

	VideoFaceDetector _detector(CASCADE_FILE, _capture);
	_detector.setResizedWidth(frameResizeWidth);

	/*if (isDebug)
	{
		cv::namedWindow(_windowName, cv::WINDOW_KEEPRATIO | cv::WINDOW_AUTOSIZE);
	}*/
	cv::Mat frame;

	//double fps = 0, time_per_frame;
	//auto start = cv::getCPUTickCount();
	_detector >> frame;
	//auto end = cv::getCPUTickCount();
	//time_per_frame = (end - start) / cv::getTickFrequency();
	//fps = (15 * fps + (1 / time_per_frame)) / 16;

	// printf("Time per frame: %3.3f\tFPS: %3.3f\n", time_per_frame, fps);

	if (_detector.isFaceFound())
	{
		/*if (isDebug)
		{
			cv::rectangle(frame, _detector.face(), cv::Scalar(255, 0, 0));
			cv::circle(frame, _detector.facePosition(), 30, cv::Scalar(0, 255, 0));
		}*/
		
		outFaces[0] = Circle(_detector.facePosition().x, _detector.facePosition().y, (_detector.face().height + _detector.face().width));
		outDetectedFacesCount++;
	}

	if (isDebug)
	{
		//cv::imshow(_windowName, frame);
	}
}